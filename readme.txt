=== TK Google Fonts ===
Contributors: svenl77, konradS
Tags: google fonts, fonts, webfonts
Requires at least: 3.5
Tested up to: 3.7.1
Stable tag: 1.2.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

TK Google Fonts is a powerful, extendable Google webfonts plugin that helps you to use Google Fonts in your theme.

== Description ==

= Fast Pageload =

The plugin only loads the fonts you select. This is good for your page loading times, as you want your site to load fast. 

= Easy to use UI =

We have build a native UI so that selecting your Google Fonts is a joy.
Fancy previews of every font and your custom text for font testing! 
Easy adding and removing of fonts with just one click.

TK Google Fonts will automatically add the selected fonts to your site. 

= Use via WordPress Customizer =

Simply use the new options that appear in the WordPress Customizer 
to apply the Google Fonts to your titles, body text or blockquotes. 

= Use via CSS = 

You can then use the fonts in your CSS without any extra step needed. 
Follow the instructions on the Google Page how to integrate your font. 

= Use within theme options =

If your theme supports TK Google Fonts in its theme options, 
your selected fonts will be offered in the theme options there! 

See a list of which themes support TK Google Fonts more down the page. 

= Customizable and extendable =

TK Google Fonts can be your Google webfonts core plugin. 

Use TK Google Fonts in your theme or plugin settings page to let the user select fonts for the different post parts like (title, text, menus,...)

All ThemeKraft Themes support TK Google Fonts 100% and every font will be immediately available in all font selectors of the theme options.

== Installation ==

= Minimum Requirements =

* WordPress 3.5 or greater
* PHP version 5.2.4 or greater
* MySQL version 5.0 or greater

= Manual installation =

The manual installation method involves downloading the plugin and uploading it to your webserver via your favourite FTP application.

1. Download the plugin file to your computer and unzip it.
2. Using an FTP program, or your hosting control panel, upload the unzipped plugin folder to your WordPress installation’s wp-content/plugins/ directory.
3. Activate the plugin from the Plugins menu within the WordPress admin panel.

= Updating =

For now manually only. This is pretty easy though. 
Delete the old plugin. Install the new one. 
See the steps for manual installation above. 

Automatic updates are coming with version 1.0

= Supported Themes =

TK Google Fonts works with every WordPress theme. 
You need to apply the new fonts via the WOrdPress Customizer or your custom CSS.  

Themes with theme options can use TK Google Fonts for their font options.
The following themes integrate TK Google Fonts in their theme options: 

* List of supported themes
	* [X2](http://www.themekraft.com/shop/x2-wordpress-theme/ "x2 WordPress Theme")

If your theme supports TK Google Fonts in its theme options but is not listend here, 
please contact us so we can add it to this list.

== Frequently Asked Questions == 

= Will TK Google Fonst work with my theme? =

Yes. TK Google Fonts will work with any theme, you can use the fonts via the WordPress Customizer or in your CSS. 

Checkout the related Google Page of the font to see how to write the CSS to call the font.  

= Where can I report bugs or contribute to the project? =

Bugs can be reported either in our support forum or preferably on the [TK Google Fonts Repository @Bitbucket](https://bitbucket.com/themekraft/tk-google-fonts/issues/ "TK Google Fonts Repository @Bitbucket").

== Screenshots ==

1. The TK Google Fonts Admin Settings Panel - images/settings-panel.(png) 

== Changelog ==

= 1.0 - 12/08/2013 =
* Initial Release. 

== Upgrade Notice == 

= 1.0 - 12/08/2013 = 
* Initial Release.
